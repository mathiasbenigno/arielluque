from flask import Flask, Response, render_template, json, request, jsonify, redirect, make_response, session
import pymysql
import pymysql.cursors
import pyquery
from pyquery import PyQuery as pq
import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import re
from datetime import datetime
from twilio.rest import Client 

app = Flask(__name__)

app.secret_key = 'abc123sosa123'


@app.route('/')
@app.route('/<name>')
def index(name=None):
    return render_template('index.html')

@app.route('/tomar_libre', methods=['POST'])
def tomar_libre():
    nombre = "CERRADO"
    fecha = request.form["fecha"]
    tipo = request.form["tipo"]
    sexo = "no"
    horario = "descanso"
    db = pymysql.connect('us-cdbr-east-03.cleardb.com',
        'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
    cursor = db.cursor()
    sql = 'INSERT INTO reservas(nombre, fecha, tipo, sexo, horario) VALUES (%s, %s, %s, %s, %s)'
    val = (nombre, fecha, tipo, sexo, horario)
    cursor.execute(sql, val)
    db.commit()
    db.close()
    return "listo"

@app.route('/check_name_consulta', methods=['POST'])
def _get_check_name_consulta():
    cadena = "^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1})?$"
    nombre = request.form["nombre"]
    print(nombre)
    if re.search(cadena, nombre) is not None:
        return "valido"
    else:
        return "no valido"


@app.route('/check_email_consulta', methods=['POST'])
def _get_check_email_consulta():
    cadenaemail = "^[0-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}[ ]{0,1}([.]{1}[a-zA-Z]{1,5})?$"
    email = request.form["email"]
    print(email)
    if re.search(cadenaemail, email) is not None:
        return "valido"
    else:
        return "no valido"


@app.route('/check_phone_consulta', methods=['POST'])
def _get_check_phone_consulta():
    cadenaphone = "^[0-9]{10}[ ]{0,1}$"
    phone = request.form["phone"]
    print(phone)
    if re.search(cadenaphone, phone) is not None:
        return "valido"
    else:
        return "no valido"


@app.route('/check_comment_consulta', methods=['POST'])
def _get_check_comment_consulta():
    cadenacomment = "^[1-9a-zA-ZñÑéÉíÍóÓúÚáÁ!.,? ]{3,280}?$"
    comment = request.form["comment"]
    print(comment)
    if re.search(cadenacomment, comment) is not None:
        return "valido"
    else:
        return "no valido"


@app.route('/submit_contact', methods=['POST'])
def _submit_contact():
    checkreg_nombre = "^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1})?$"
    checkreg_phone = "^[0-9]{10}[ ]{0,1}$"
    checkreg_phone_chile = "^[+]{1}[5]{1}[6]{1}[0-9]{9,9}$"
    checkreg_email = "^[0-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}[ ]{0,1}([.]{1}[a-zA-Z]{1,5}[ ]{0,1})?$"
    nombre = request.form["nombre"]
    email = request.form["email"]
    checkreg_comment = "^[0-9a-zA-ZñÑéÉíÍóÓúÚáÁ¿¡!$#@.,? ]{3,280}?$"
    phone = request.form["phone"]
    comment = request.form["comment"]
    print("Servidor acaba de recibir un intento de contacto via web...")
    if re.search(checkreg_nombre, nombre) is None or nombre == '' or re.search(checkreg_email, email) is None or re.search(checkreg_phone, phone) is None or phone == '' or re.search(checkreg_comment, comment) is None or comment == '':
        return "no valido"
    else:
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        cursor = db.cursor()
        print("buscando si los datos no estan en la tabla datos")
        data_search = cursor.execute(
            'SELECT * FROM datos WHERE email=%s;', (email))

        db.commit()
        if(data_search == 0):
            print("no hay datos en datos")
            sql = 'INSERT INTO datos(nombre, telefono, email, desde) VALUES (%s, %s, %s, %s)'
            val = (nombre, phone, email, "contacto")
            cursor.execute(sql, val)
            db.commit()
            db.close()

        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        cursor = db.cursor()

        sql = 'INSERT INTO consultas(nombre, telefono, email, comentario) VALUES (%s, %s, %s, %s)'
        val = (nombre, phone, email, comment)
        cursor.execute(sql, val)
        db.commit()
        db.close()

        mailServer = smtplib.SMTP('smtp.gmail.com', 587)
        mailServer.ehlo()
        data = "Se acaba de realizar una consulta via web en la seccion contacto. \n" + "El nombre es: " + nombre + "\n" + "Su email es: " + email + "\n" + "Su numero de tel es: " + \
            phone + "\n" + "El comentario realizado fue el siguiente: " + comment + "\n" + \
            "\n" + "Eso es todo, recuerda responder al correo del cliente, no a este :)"
        data_user = "Recibimos tu consulta con exito, por favor no respondas este email, pronto te estaremos respondiendo desde nuestro correo! \n saludos"
        mailServer.starttls()
        mailServer.ehlo()
        mailServer.login('arielluqueapp@gmail.com', '@Arielluque123')
        mensaje = MIMEText(data)
        mensaje['From'] = 'arielluqueapp@gmail.com'
        mensaje['To'] = 'ariel.luquecorreos@gmail.com'
        mensaje['Subject'] = 'Contacto Ariel Luque Peluqueria'

        mensaje_user = MIMEText(data_user)
        mensaje_user['From'] = 'arielluqueapp@gmail.com'
        mensaje_user['To'] = email
        mensaje_user['Subject'] = 'Contacto desde Ariel Luque Peluqueria'

        mailServer.send_message(mensaje_user)
        mailServer.send_message(mensaje)
        print("se envio la consulta")

        return "valido"


@app.route('/ver_fecha', methods=['POST'])
def ver_fecha():
    print("entro en funcion ver fecha")
    #empleado = request.form['empleado']#
    fecha = request.form['fecha']
    horario = "descanso"
    db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                        'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
    cursor = db.cursor()
    cursor2 = db.cursor()
    sql = 'SELECT horario FROM reservas WHERE fecha = %s'
    val = (fecha)
    print(fecha)
    buscador = cursor2.execute(sql, val)
    sql2 = 'SELECT horario FROM reservas WHERE fecha = %s AND horario=%s'
    val2 = (fecha, horario)
    dialibre = cursor.execute(sql2, val2)
    print("dia libre")
    print(dialibre)
    print ("s")
    print(buscador)
    print ("x")
    db.commit()
    if (dialibre == 1):
        print("hay descanso")
        return "hay descanso"

    else:
        print ("no hay descanso")
        if (buscador == 0):
            print("entro en el if 0")
            return 'no hay'
        else:

            print("entro en el else")
            result = cursor2.fetchall()

            print("llego al fetchall")
            # diccionario = dict(enumerate(set(result)))

            formatear = '{{"valido" : "okay", "result" : "{}"}}'.format(result)

            z = json.loads(formatear)
            # w = "{{'respuesta' : 'okay', 'result' : '{}'}}".format(diccionario)

            # z = json.loads(w)
            # print(z)

            print("llego al z")
            return z


@app.route('/datos_reserva', methods=['POST'])
def datos_reserva():
    checkreg_nombre = '^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1})?$'
    checkreg_phone = '^[0-9]{10}[ ]{0,1}$'
    checkreg_phone_ch = '^[+]{1}[5]{1}[6]{1}[0-9]{9,9}$'
    checkreg_email = '^[0-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5}[ ]{0,1})?$'
    nombre = request.form['nombre']
    email = request.form['email']
    phone = request.form['tel']
    sexo = request.form['sexo']
    pago = request.form['pago']
    precio = request.form['precio']
    servicio = request.form['servicio']
    #empleado = request.form['empleado']#
    fecha = request.form['fecha']
    horario = request.form['horario']
   
    print("cargo las variables")
    print(horario)
    if re.search(checkreg_nombre, nombre) is None or nombre == '' or re.search(checkreg_phone, phone) is None or phone == '':
        return 'no valido'
    else:

        if (horario == "09:00"):
            horarioid = "A"
        elif(horario == "09:30"):
            horarioid = "B"
        elif(horario == "10:00"):
            horarioid = "C"
        elif(horario == "10:30"):
            horarioid = "D"
        elif(horario == "11:00"):
            horarioid = "E"
        elif(horario == "11:30"):
            horarioid = "F"
        elif(horario == "12:00"):
            horarioid = "G"
        elif(horario == "12:30"):
            horarioid = "H"
        elif(horario == "13:00"):
            horarioid = "I"
        elif(horario == "13:30"):
            horarioid = "J"
        elif(horario == "14:00"):
            horarioid = "K"
        elif(horario == "14:30"):
            horarioid = "L"
        elif(horario == "15:00"):
            horarioid = "M"
        elif(horario == "15:30"):
            horarioid = "N"
        elif(horario == "16:00"):
            horarioid = "O"
        elif(horario == "16:30"):
            horarioid = "P"            
        elif(horario == "17:00"):
            horarioid = "Q"            
        elif(horario == "17:30"):
            horarioid = "R"            
        elif(horario == "18:00"):
            horarioid = "S"            
        elif(horario == "18:30"):
            horarioid = "T"            
        elif(horario == "19:00"):
            horarioid = "U"            
        elif(horario == "19:30"):
            horarioid = "V"            
        elif(horario == "20:00"):
            horarioid = "W"            

        print(horarioid)
        print("entro en el else")
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print("conecto con la base de datos")
        cursor = db.cursor()
        data_search = cursor.execute(
            'SELECT * FROM reservas WHERE id_horario=%s AND fecha=%s;', (horarioid, fecha))
        db.commit()
        db.close()
        if (data_search == 0):

            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
            print("conecto con la base de datos")
            cursor = db.cursor()
            sql = 'INSERT INTO reservas(nombre, telefono, email, sexo, pago, precio, servicio, fecha, horario, id_horario) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            val = (nombre, phone, email, sexo, pago, precio,
               servicio, fecha, horario, horarioid)
            cursor.execute(sql, val)
            db.commit()

            print("buscando si los datos no estan en la tabla datos")
            data_search = cursor.execute(
              'SELECT * FROM datos WHERE telefono=%s;', (phone))

            db.commit()
            if(data_search == 0):
                print("no hay datos en datos")
                sql = 'INSERT INTO datos(nombre, telefono, email, desde) VALUES (%s, %s, %s, %s)'
                val = (nombre, phone, email, "reserva online")
                cursor.execute(sql, val)
                db.commit()
                db.close()
            return 'valido'
        else:

            return 'esta ocupado'


@app.route('/deletecontacto', methods=['POST'])
def deletecontacto():
    id = request.form['id']
    username = request.form["username"]
    password = request.form["password"]
    tipo = request.form["tipo"]
    print("se ejecuto la funcion deletecontacto y el id es " + id)
    if (tipo == "admin"):

        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")

        cursor = db.cursor()

        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (username, password))
        db.commit()
        db.close()
        if (count == 0):
            return "no nimda"
        else:
            if (id == ''):
                print("id esta vacia")
                return "id vacia"
            else:
                db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

                print(
                    "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")

                cursor = db.cursor()

                delete = cursor.execute(
                    'DELETE FROM consultas WHERE id=%s;', (id))
                db.commit()
                db.close()
                return "deleted"
    else:
        return "np"


@app.route('/deletereserva', methods=['POST'])
def deletereserva():
    id = request.form['id']
    print("se ejecuto la funcion deletereserva y el id es " + id)
    if id == '':
        print("id esta vacia...")
        return "id vacia"
    else:
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")

        cursor = db.cursor()

        delete = cursor.execute(
            'DELETE FROM reservas WHERE id=%s;', (id))
        db.commit()
        db.close()
        return "deleted"


@app.route('/_get_order_to_send', methods=['POST'])
def get_order():

    nombre = request.form['nombre']
    email = request.form['email']
    phone = request.form['tel']
    # sexo = request.form['sexo']
    pago = request.form['pago']
    precio = request.form['precio']
    servicio = request.form['servicio']
    empleado = request.form['empleado']
    fecha = request.form['fecha']
    horario = request.form['horario']
    # data = request.data

    print('se accedio a get order')
    # print(data)
    data = "Hola " + nombre + ", gracias por reservar con Ariel Luque Peluqueria para Caballeros. \n " + "Tu reserva seria para el dia " + fecha + ", en el horario de " + horario + ", nos indicaste que pagarias con " + pago + \
        ", " + " \n. te estaremos esperando!" + "\n" + \
        "Cualquier otra consulta, no respondas a este correo, envianos tu consulta via web, seccion 'contacto', o a traves de nuestro whatsapp: +5493492322020" + \
        " \n Saludos, att Ariel Luque Peluqueria."

    data2 = "Se acaba de realizar una reserva para el dia " + fecha + ", el horario seria: " + horario + ", del cual el metodo de pago seleccionado fue: " + pago + \
            ", el nombre del cliente es: " + nombre + ", su correo es: " + \
        email + " y su numero de telefono es: " + phone

    mailServer = smtplib.SMTP('smtp.gmail.com', 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login('arielluqueapp@gmail.com', '@Arielluque123')
    mensaje = MIMEText(data2)
    mensaje['From'] = 'arielluqueapp@gmail.com'
    mensaje['To'] = 'ariel.luquecorreos@gmail.com'
    mensaje['Subject'] = 'Reserva Ariel Luque Peluqueria'
    mailServer.send_message(mensaje)

    # segundo destinatario

    mailServer = smtplib.SMTP('smtp.gmail.com', 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login('arielluqueapp@gmail.com', '@Arielluque123')
    mensaje = MIMEText(data)
    mensaje['From'] = 'arielluqueapp@gmail.com'
    mensaje['To'] = email
    mensaje['Subject'] = 'Reserva Ariel Luque Peluqueria'
    mailServer.send_message(mensaje)


    account_sid = 'AC89ead8c268ae67f7a6705f756b0824e1' 
    auth_token = '9a31c0e777ccbea7a064da69e3454a0b' 
    client = Client(account_sid, auth_token) 
 
    message = client.messages.create( 
                              from_='whatsapp:+14155238886',  
                              body=data2,      
                              to='whatsapp:+5493492322020' 
                          ) 
 
    print(message.sid)
    return 'ok'


@app.route('/_get_contacto', methods=['POST'])
def _get_contact():
    return jsonify({'data': render_template('contacto.html')})


@app.route('/_get_fotos', methods=['POST'])
def _get_fotos():
    return jsonify({'data': render_template('fotos.html')})


@app.route('/_get_inicio', methods=['POST'])
def _get_inicio():

    return jsonify({'data': render_template('home.html')})


@app.route('/_get_reservar', methods=['POST'])
def _get_reservar():

    return jsonify({'data': render_template('reservar.html')})


@app.route('/_get_login', methods=['POST'])
def _get_login():

    return jsonify({'data': render_template('login.html')})


@app.route('/dwdb', methods=['POST'])
def dwdb():
    username = request.form["username"]
    password = request.form["password"]
    tipo = request.form["tipo"]
    if (tipo == "admin"):
        print("Se esta accediendo accediendo a la base de datos para descargar...")
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()
        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (username, password))
        print("hizo query")
        if (count == 0):
            print("count 0")
            return "false 0"
        else:
            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
            cursor = db.cursor()
            count = cursor.execute('SELECT * FROM datos;')
            datos_table = cursor.fetchall()
            db.commit()
            db.close()
            JsonDatos = []
            num = 0
            datosx = "datos"

            for variable in datos_table:
                numb = num
                num = int(num) + 1
                print(num)
                Datox = datosx + str(num)
                dato = {
                    Datox: [
                        {'id': datos_table[numb][0]},
                        {'nombre': datos_table[numb][1]},
                        {'email': datos_table[numb][2]},
                        {'telefono': datos_table[numb][3]},
                        {'desde': datos_table[numb][4]}
                    ]
                }
                JsonDatos.append(dato)
            z = json.dumps(JsonDatos)
            print(z)
            return z
    else:
        return "false"


@app.route('/vwconsult', methods=['POST'])
def vwconsult():
    print("vwconsult")
    username = request.form["username"]
    password = request.form["password"]
    tipo = request.form["tipo"]
    if (tipo == "admin"):
        print("Se esta accediendo accediendo a la base de datos para descargar...")
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()
        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (username, password))
        print("hizo query")
        if (count == 0):
            print("count 0")
            return "false 0"
        else:
            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
            cursor = db.cursor()
            count = cursor.execute('SELECT * FROM consultas;')
            datos_table = cursor.fetchall()
            db.commit()
            db.close()
            JsonDatos = []
            num = 0
            datosx = "datos"

            for variable in datos_table:
                numb = num
                num = int(num) + 1
                print(num)
                Datox = datosx + str(num)
                dato = {
                    Datox: [
                        {'id': datos_table[numb][0]},
                        {'nombre': datos_table[numb][1]},
                        {'email': datos_table[numb][2]},
                        {'telefono': datos_table[numb][3]},
                        {'comentario': datos_table[numb][4]}
                    ]
                }
                JsonDatos.append(dato)
            z = json.dumps(JsonDatos)
            print(z)
            return z
    else:
        return "false"


@app.route('/givemeidhorario', methods=['POST'])
def givemeidhorario():
    id = request.form["id_horario"]
    if id == '':
        print("Alguno o ambos input del login estan vacios")
        return "no valido"
    else:
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")

        cursor = db.cursor()

        count = cursor.execute(
            'select * from reservas where id_horario=%s;', (id))
        id_reserva = cursor.fetchall()
        db.commit()
        db.close()
        return "asd"


@app.route('/whowatch', methods=['POST'])
def whowatch():
    usuario = request.form["username"]
    password = request.form["password"]
    tipo = request.form["tipo"]
    empleado = request.form["whowatch"]
    if usuario == '' or password == '':
        print("Alguno o ambos input del login estan vacios")
        return "no valido"
    elif (tipo == 'admin'):
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()

        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (usuario, password))

        db.commit()
        db.close()

        # print(formatear)

        # print(z)
        if count == 0:
            print("Usuario no existe")
            return "Usuario no existe"
        else:
            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

            cursor = db.cursor()
            if (empleado == "Todos"):
                count = cursor.execute(
                    'SELECT * FROM reservas ORDER BY fecha ASC, id_horario ASC LIMIT 100 OFFSET 0')
            else:
                count = cursor.execute(
                    'SELECT * FROM reservas WHERE empleado = %s ORDER BY fecha ASC, id_horario ASC LIMIT 100 OFFSET 0', (empleado))

            print("Trayendo las fechas y horarios reservados")
            reservas = cursor.fetchall()
            db.commit()
            db.close()
            if (count == 0):
                return "no hay reservas"
            else:
                # print(reservas[3][4])
                thelast2 = []
                num = 0
                fecha = "fecha"

                for variable in reservas:
                    numb = num
                    num = int(num) + 1

                    print(num)

                    Fecha = fecha + str(num)
                    afecha = reservas[numb][4]
                    afecha = str(afecha)
                    # newDate = datetime.strptime(afecha, "%y/%m")
                    # print(newDate)
                    # fechax = datetime.strptime(reservas[numb][4], "Y-mm-dd").strftime("%dd-%mm-%yy")
                    # print(fechax)
                    dato = {
                        Fecha: [
                            {'Id': reservas[numb][0]},
                            {'Nombre': reservas[numb][1]},
                            {'telefono': reservas[numb][2]},
                            {'email': reservas[numb][3]},
                            {'fecha': afecha},
                            {'horario': reservas[numb][5]},
                            {'servicio': reservas[numb][6]},
                            {'sexo': reservas[numb][7]},
                            {'precio': reservas[numb][8]},
                            {'pago': reservas[numb][9]},
                            {'empleado': reservas[numb][10]},
                            {'id_horario': reservas[numb][11]}
                        ]
                    }

                    thelast2.append(dato)

                # last_fechas = '{{"fecha1" : [{"id" : "{0}"}], "fecha2" : "{1}", "fecha3" : "{2}", "fecha4" : "{3}", "fecha5" : "{4}", "fecha6" : "{5}", "fecha7" : "{6}", "fecha8" : "{7}", "fecha9" : "{8}", "fecha10" : "{9}"}}'.format(
                # reservas[0], reservas[1], reservas[2], reservas[3], reservas[4], reservas[5], reservas[6], reservas[7], reservas[8], reservas[9])
                z = json.dumps(thelast2)

                # print(z["valido"])
                return z


@app.route('/givemedates', methods=['POST'])
def givemedates():
    usuario = request.form["username"]
    password = request.form["password"]
    tipo = request.form["tipo"]
    empleado = request.form["nombre"]
    if usuario == '' or password == '':
        print("Alguno o ambos input del login estan vacios")
        return "no valido"
    elif (tipo == 'user'):
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()

        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (usuario, password))

        db.commit()
        db.close()

        # print(formatear)

        # print(z)
        if count == 0:
            print("Usuario no existe")
            return "Usuario no existe"
        else:
            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

            cursor = db.cursor()

            count = cursor.execute(
                'SELECT * FROM reservas WHERE empleado = %s ORDER BY fecha ASC, id_horario ASC LIMIT 100 OFFSET 0', (empleado))

            print("Trayendo las fechas y horarios reservados")
            reservas = cursor.fetchall()
            db.commit()
            db.close()
            if (count == 0):
                return "no hay reservas"
            else:
                # print(reservas[3][4])
                thelast2 = []
                num = 0
                fecha = "fecha"

                for variable in reservas:
                    numb = num
                    num = int(num) + 1

                    print(num)

                    Fecha = fecha + str(num)
                    afecha = reservas[numb][4]
                    afecha = str(afecha)
                    # newDate = datetime.strptime(afecha, "%y/%m")
                    # print(newDate)
                    # fechax = datetime.strptime(reservas[numb][4], "Y-mm-dd").strftime("%dd-%mm-%yy")
                    # print(fechax)
                    dato = {
                        Fecha: [
                            {'Id': reservas[numb][0]},
                            {'Nombre': reservas[numb][1]},
                            {'telefono': reservas[numb][2]},
                            {'email': reservas[numb][3]},
                            {'fecha': afecha},
                            {'horario': reservas[numb][5]},
                            {'servicio': reservas[numb][6]},
                            {'sexo': reservas[numb][7]},
                            {'precio': reservas[numb][8]},
                            {'pago': reservas[numb][9]},
                            {'empleado': reservas[numb][10]},
                            {'id_horario': reservas[numb][11]}
                        ]
                    }

                    thelast2.append(dato)

                # last_fechas = '{{"fecha1" : [{"id" : "{0}"}], "fecha2" : "{1}", "fecha3" : "{2}", "fecha4" : "{3}", "fecha5" : "{4}", "fecha6" : "{5}", "fecha7" : "{6}", "fecha8" : "{7}", "fecha9" : "{8}", "fecha10" : "{9}"}}'.format(
                # reservas[0], reservas[1], reservas[2], reservas[3], reservas[4], reservas[5], reservas[6], reservas[7], reservas[8], reservas[9])
                z = json.dumps(thelast2)

                # print(z["valido"])
                return z
    else:
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()

        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (usuario, password))

        db.commit()
        db.close()

        # print(formatear)

        # print(z)
        if count == 0:
            print("Usuario no existe")
            return "Usuario no existe"
        else:
            db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')

            cursor = db.cursor()

            count = cursor.execute(
                'SELECT * FROM reservas ORDER BY fecha ASC, id_horario ASC')

            print("Trayendo las fechas y horarios reservados")
            reservas = cursor.fetchall()
            db.commit()
            db.close()
            if (count == 0):
                return "no hay reservas"
            else:
                # print(reservas[3][4])
                thelast2 = []
                num = 0
                fecha = "fecha"

                for variable in reservas:
                    numb = num
                    num = int(num) + 1

                    print(num)

                    Fecha = fecha + str(num)
                    afecha = reservas[numb][4]
                    afecha = str(afecha)
                    # newDate = datetime.strptime(afecha, "%y/%m")
                    # print(newDate)
                    # fechax = datetime.strptime(reservas[numb][4], "Y-mm-dd").strftime("%dd-%mm-%yy")
                    # print(fechax)
                    dato = {
                        Fecha: [
                            {'Id': reservas[numb][0]},
                            {'Nombre': reservas[numb][1]},
                            {'telefono': reservas[numb][2]},
                            {'email': reservas[numb][3]},
                            {'fecha': afecha},
                            {'horario': reservas[numb][5]},
                            {'servicio': reservas[numb][6]},
                            {'sexo': reservas[numb][7]},
                            {'precio': reservas[numb][8]},
                            {'pago': reservas[numb][9]},
                            {'empleado': reservas[numb][10]},
                            {'id_horario': reservas[numb][11]}
                        ]
                    }

                    thelast2.append(dato)

                # last_fechas = '{{"fecha1" : [{"id" : "{0}"}], "fecha2" : "{1}", "fecha3" : "{2}", "fecha4" : "{3}", "fecha5" : "{4}", "fecha6" : "{5}", "fecha7" : "{6}", "fecha8" : "{7}", "fecha9" : "{8}", "fecha10" : "{9}"}}'.format(
                # reservas[0], reservas[1], reservas[2], reservas[3], reservas[4], reservas[5], reservas[6], reservas[7], reservas[8], reservas[9])
                z = json.dumps(thelast2)

                # print(z["valido"])
                return z


@app.route('/login_access', methods=['POST'])
def login_access():
    usuario = request.form["username"]
    password = request.form["password"]
    print("se conecto el usuario: " + '"' + usuario +
          '"' + ", su password es: " + '"' + password + '"')
    if usuario == '' or password == '':
        print("Alguno o ambos input del login estan vacios")
        return "no valido"
    else:
        db = pymysql.connect('us-cdbr-east-03.cleardb.com',
                             'bc84b3d706c610', 'c824b9b5', 'heroku_0435413f4d50f46')
        print(
            "Verificando si el usuario ingresado y contraseña coinciden con uno existente!")
        cursor = db.cursor()

        count = cursor.execute(
            'select * from empleados where username=%s AND password=%s;', (usuario, password))
        cursor.execute(
            'select nombre, tipo, password, username from empleados where username=%s;', (usuario))
        result = cursor.fetchall()

        # este trae un solo resultado /// result = cursor.fetchone()
        # este trae todos los  resultado /// result = cursor.fetchall()
        result = result[0]
        # categoria= result[1]
        # print(result)
        # session['categoria'] = result[1]

        # array = ["login valido", "login no valido", result]
        # diccionario = dict(enumerate(set(result)))

        # resultado = sorted(diccionario.items(), key=operator.itemgetter(0))

        # print(resultado)
        x = '{{"no valido": "login no valido", "valido" : "login valido", "result" : "{0}"}}'.format(
            result)
        y = json.loads(x)
        # w = '{{"valido" : "login valido", "result" : "{0}"}}'.format(result)
        # z = json.loads(w)
        formatear = '{{"valido" : "login valido", "nombre" : "{0}", "tipo" : "{1}", "password" :"{2}", "username" : "{3}"}}'.format(
            result[0], result[1], result[2], result[3])
        print(formatear)
        z = json.loads(formatear)
        # print(formatear)

        # print(z)
        if count == 0:
            print(y["no valido"])
            return y["no valido"]
        else:
            session['usuario'] = usuario
            session['password'] = password
            # print(z["valido"])
            return z


@app.route('/check_name_reserva', methods=['POST'])
def _get_check_name_reserva():
    cadena = '^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}[ ]{0,1})?$'
    nombre = request.form['nombre']
    print(nombre)
    if re.search(cadena, nombre) is not None:
        session['nombre_reserva'] = nombre
        return 'valido'
    else:
        return 'no valido'


@app.route('/check_email_reserva', methods=['POST'])
def _get_check_email_reserva():
    cadenaemail = '^[0-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5}[ ]{0,1})?$'
    email = request.form['email']
    print(email)
    if re.search(cadenaemail, email) is not None:
        session['email_reserva'] = email
        return 'valido'
    else:
        return 'no valido'


@app.route('/check_phone_reserva', methods=['POST'])
def _get_check_phone_reserva():
    cadenaphone = '^[0-9]{10}[ ]{0,1}$'
    phone = request.form['phone']
    print(phone)
    if re.search(cadenaphone, phone) is not None:
        session['tel_reserva'] = phone
        return 'valido'
    else:
        return 'no valido'


if __name__ == '__main__':
    app.run(debug='false')
